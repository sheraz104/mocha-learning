import MyClass from '../src/myClass';
import sinon from 'sinon';

require('chai')
  .use(require('chai-as-promised'))
  .should();

describe('MyClass', () => {
  before(function () {
    this.myClass = new MyClass();
  });

  afterEach(function () {
    sinon.restore();
  });

  it('test add method', function () {
    const {arg1, arg2} = this.myClass;
    const want = arg1 + arg2;
    const got = this.myClass
      .add(arg1, arg2);

    got.should.be.equal(want);
  });

  it('spy add method', function () {
    const {arg1, arg2} = this.myClass;

    const spyAdd = sinon.spy(this.myClass, 'add');
    this.myClass.addWrapper(arg1, arg2);

    spyAdd.calledOnce.should.be.true;
    spyAdd.calledWith(arg1, arg2)
      .should.be.true;
  });

  it('spy the callback', function () {
    const cb = sinon.spy();
    this.myClass.callBack(cb);

    cb.calledOnce.should.be.true;
  });

  it('mock methods from MyClass', function () {
    const {arg1, arg2} = this.myClass;

    const mock = sinon.mock(this.myClass);
    const mockFuncOne = mock.expects('mockMethodOne');
    const mockFuncTwo = mock.expects('mockMethodTwo');

    mockFuncOne.exactly(1);
    mockFuncOne.withArgs('Hello, world!');

    mockFuncTwo.exactly(1);
    mockFuncTwo.withArgs('Hello, Sheraz!');

    this.myClass.addWrapper(arg1, arg2);

    mockFuncOne.verify();
    mockFuncTwo.verify();
  });

  it('test stub for add', function () {
    const {arg1, arg2} = this.myClass;
    const addStub = sinon.stub(this.myClass, 'add');

    const want = 100;
    addStub.withArgs(arg1, arg2)
      .returns(want);

    const got = this.myClass
      .addWrapper(arg1, arg2);

    got.should.be.equal(want);
  });

  it('test promise with number', function () {
    this.myClass.testPromise()
      .should.eventually.equal(250);
  });

  it('test promise with fulfillment', function () {
    this.myClass.testPromise()
      .should.be.fulfilled;
  });
});
