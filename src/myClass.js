export default class MyClass {
  constructor () {
    this._arg1 = 3;
    this._arg2 = 5;
  }

  set arg1 (_value) {
    this._arg1 = _value;
  }

  get arg1 () {
    return this._arg1;
  }

  set arg2 (_value) {
    this._arg2 = _value;
  }

  get arg2 () {
    return this._arg2;
  }

  add (arg1, arg2) {
    return arg1 + arg2;
  }

  addWrapper (arg1, arg2) {
    this.mockMethodOne('Hello, world!');
    this.mockMethodTwo('Hello, Sheraz!');

    return this.add(arg1, arg2);
  }

  callBack (cb) {
    cb();
  }

  mockMethodOne (str) {
    // eslint-disable-next-line no-console
    console.log(str);
  }

  mockMethodTwo (str) {
    // eslint-disable-next-line no-console
    console.log(str);
  }

  testPromise () {
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      setTimeout(
        () => resolve(250, 2000)
      );
    });
  }
}
